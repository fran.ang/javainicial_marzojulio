
package paquete;


public class Perro {
    
    //ATRIBUTOS SON VARIABLES DE CLASE.
    //Sintaxis: modificador tipoVariable nombreVariable;
    public String raza;
    public String color;
    public double peso;
    public int edad;
    public int nroPerro; // + nroPerro :int
    
    //COMPORTAMIENTO SON METODOS EN JAVA
    //Sintaxis: modificador retornoMetodo nombreMetodo()
    public void ladrar(){
        System.out.println("Guau guau!");
    }
    
    public void dormir(){
        System.out.println("ZzZzZ");
    }
    
    public void comer(){
        //sout + TAB
        System.out.println("Alimentame humano!");
    }
    
    public String toString(){
        String cadena = "Raza: " + raza + 
                        ", Color: " + color + 
                        ", Peso: " + peso + 
                        ", Edad: " + edad +
                        ", Nro. Perro: " + nroPerro;
        
        return cadena;
    }
    
} //Termina la clase
