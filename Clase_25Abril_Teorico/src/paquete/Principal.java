
package paquete;


public class Principal {


    public static void main(String[] args) {
        
        //Sintaxis: Clase nombreObj = new Clase();
        Perro objToby = new Perro();
        
        objToby.raza = "Labrador";
        objToby.peso = 3.55;
        objToby.nroPerro = 3347;
        objToby.color = "Amarillo";
        objToby.edad = 8;
        
        String infoToby = objToby.toString(); //Metodo con retorno
        
        System.out.println("La informacion de Toby: " + infoToby);
        
        objToby.ladrar(); //Los void no se guardan en variables
        
    } //Termina el main
    
} //Termina la clase