
package paquete;

public class Principal {

    public static void main(String[] args) {
       
        //Primer objeto
        //Sintaxis: Clase nomObj = new Clase();
        
        Articulo auriculares = new Articulo(1, "Auriculares", 12500, false);
        
        String infoAuri = auriculares.toString();
        System.out.println("Info Auriculares: " + infoAuri);
        
        float precioAuri = auriculares.getPrecio();
        System.out.println("Precio SIN impuesto: " + precioAuri);
        
        float precioAuriImp = auriculares.calcularPrecioFinal();
        System.out.println("Precio CON impuesto: " + precioAuriImp);
        
        //SEGUNDO OBJETO
        Articulo ventilador = new Articulo(2, "Ventilador", 14000, true);
        
        String infoVenti = ventilador.toString();
        System.out.println("Info Ventilador: " + infoVenti);
        
        //TERCER OBJETO
        Articulo balanza = new Articulo();
        String infoBalanza = balanza.toString();
        System.out.println("Info Balanza: " + infoBalanza);
        
        
    } //Fin del main
    
} //Fin de la clase
