
package paquete;


public class Articulo {
    
    //ATRIBUTOS O VARIABLES DE LA CLASE
    private int idArticulo;
    private String nombre;
    private float precio;
    private boolean estaVendido;
    
    //CONSTRUCTORES
    public Articulo(){
        idArticulo = -1;
        nombre = "NO DEFINIDO";
        precio = -1;
        estaVendido = false;
    }
    
    public Articulo(int nvoId, String nvoNombre, float nvoPrec, boolean nvoVend){
        idArticulo = nvoId;
        nombre = nvoNombre;
        precio = nvoPrec;
        estaVendido = nvoVend;
    }
    
    //COMPORTAMIENTOS O METODOS DE LA CLASE
    public int getIdArticulo(){
        return idArticulo;
    }
    
    public void setIdArticulo(int nuevoId){
        idArticulo = nuevoId;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public void setNombre(String nuevoNombre){
        nombre = nuevoNombre;
    }
    
    public float getPrecio(){
        return precio;
    }
    
    public void setPrecio(float nuevoPrecio){
        precio = nuevoPrecio;
    }
    
    public boolean getEstaVendido(){
        return estaVendido;
    }
    
    public void setEstaVendido(boolean nuevoVendido){
        estaVendido = nuevoVendido;
    }
    
    public float calcularPrecioFinal(){
        float precioF = 0.0f;
        float impuesto = precio * 0.21f;
        precioF = precio + impuesto;
        
        return precioF;
    }
    
    
    public String toString(){
        String cadena = "ID Articulo: " + idArticulo 
                + ", Nombre: " + nombre
                + ", Precio: " + precio + "$"
                + ", ¿Esta vendido? " + estaVendido;
        
        return cadena;
    }
    
    
} //Fin de la clase
