package paquete;

public class Principal {
    
    //VARIABLE DE LA CLASE
    public static int menor = -1; //Guarda el menor de los numeros
    
    public static void main(String[] args) {
        
        //SUBPROBLEMA 1
        buscarMenor();
        
        //SUBPROBLEMA 2
        calcular();
                        
    } //Termina el main
    
    public static void buscarMenor(){
        //ENTRADAS
        int nro1 = 200;
        int nro2 = 100;
        int nro3 = 30;
                        
        //PROCESO
        if( nro1 < nro2 && nro1 < nro3 ){
            menor = nro1; //Asginar un nuevo valor         
        }else if( nro2 < nro3 ){
            menor = nro2;
        }else{
            menor = nro3;            
        }
        
        //SALIDA
        System.out.println("El menor es: " + menor);
    }
    
    public static void calcular(){
        int cuad = menor * menor;
        int cubo = cuad * menor;
        
        System.out.println("El cuadrado es: " + cuad);
        System.out.println("El cubo es: " + cubo);
    }
    
} //Termina la clase
