package paquete;


public class GestorAlumno {

    public GestorAlumno() {
    }
    
    public int mostrarNotaMasAlta(Alumno alum){
        int notaAlta = 0;
        
        int nota1 = alum.getNota1();
        int nota2 = alum.getNota2();
        int nota3 = alum.getNota3();
        
        if( nota1 > nota2 && nota1 > nota3 ){
            notaAlta = nota1;
        } else if(nota2 > nota3){
            notaAlta = nota2;
        } else{
            notaAlta = nota3;
        }
        
        return notaAlta;
    }
    
    //TAREA
    public int mostrarNotaMasBaja(Alumno alum){
        int notaBaja = 0;
        
        int nota1 = alum.getNota1();
        int nota2 = alum.getNota2();
        int nota3 = alum.getNota3();
        
        if( nota1 < nota2 && nota1 < nota3 ){
            notaBaja = nota1;
        } else if(nota2 < nota3){
            notaBaja = nota2;
        } else{
            notaBaja = nota3;
        }
        
        return notaBaja;
    }
    
    public float mostrarPromedioNota(Alumno alum){
        float promedio = 0;
        
        float nota1 = alum.getNota1();
        float nota2 = alum.getNota2();
        float nota3 = alum.getNota3();
        
        float suma = nota1 + nota2 + nota3;
        promedio = suma / 3;
        
        return promedio;
    }
    
    public float mostrarPromedioNotaArreglo(Alumno alum){
        float promedio = 0;
        
        //Declarar e instanciar el arreglo
        int notas[] = new int[3];
        notas[0] = alum.getNota1();
        notas[1] = alum.getNota2();
        notas[2] = alum.getNota3();
        
        //Promedio = Suma de las notas / Cantidad de notas
        int cantNotas = notas.length;
        float suma = 0;
        
        for(int nota : notas){
            suma += nota; //suma = suma + nota;
        }
        
        promedio = suma / cantNotas;
        
        return promedio;
    }
    
    //TAREA
    
    //Mostrar la nota mas alta de dos alumnos
    public int mostrarNotaMasAltaAmbos(Alumno alum1, Alumno alum2){
        int notaAlta = 0;
        
        int notaMay1 = mostrarNotaMasAlta(alum1);
        int notaMay2 = mostrarNotaMasAlta(alum2);
        
        if(notaMay1 > notaMay2){
            notaAlta = notaMay1;
        }else{
            notaAlta = notaMay2;
        }
        
        return notaAlta;
    }
    
    //Mostrar el nombre del alumno con la nota mas alta
    public String mostrarAlumnoNotaAlta(Alumno alum1, Alumno alum2){
        String nombreAlumno = "";
        
        int notaMay1 = mostrarNotaMasAlta(alum1);
        int notaMay2 = mostrarNotaMasAlta(alum2);
        
        if(notaMay1 == notaMay2){
            nombreAlumno = "Tanto " +  alum1.getNombre() + " como " + alum2.getNombre() + " tienen notas altas.";
        }
        else if(notaMay1 > notaMay2){
            nombreAlumno = alum1.getNombre();
        }else{
            nombreAlumno = alum2.getNombre();
        }
        
        return nombreAlumno;
    }
    
}
