package paquete;

public class Principal {

    public static void main(String[] args) {
        
        //Primer alumno
        System.out.println("CARGA DEL PRIMER ALUMNO");
        System.out.print("Ingresar el nombre del alumno: ");
        String nombreAlum1 = Consola.readLine();
        
        System.out.print("Ingresar su primera nota: ");
        int nota1Alum1 = Consola.readInt();
        
        System.out.print("Ingresar su segunda nota: ");
        int nota2Alum1 = Consola.readInt();
        
        System.out.print("Ingresar su tercera nota: ");
        int nota3Alum1 = Consola.readInt();
        
        Alumno alum1 = new Alumno(nombreAlum1, nota1Alum1, nota2Alum1, nota3Alum1);
        
        //Segundo alumno
        System.out.println("CARGA DEL SEGUNDO ALUMNO");
        System.out.print("Ingresar el nombre del alumno: ");
        String nombreAlum2 = Consola.readLine();
        
        System.out.print("Ingresar su primera nota: ");
        int nota1Alum2 = Consola.readInt();
        
        System.out.print("Ingresar su segunda nota: ");
        int nota2Alum2 = Consola.readInt();
        
        System.out.print("Ingresar su tercera nota: ");
        int nota3Alum2 = Consola.readInt();
        
        Alumno alum2 = new Alumno(nombreAlum2, nota1Alum2, nota2Alum2, nota3Alum2);
        
        System.out.println( "Primer alumno: " + alum1.toString() );
        System.out.println( "Segundo alumno: " + alum2.toString() );
        
        GestorAlumno gestor = new GestorAlumno();
        
        int notaMayorAlum1 = gestor.mostrarNotaMasAlta(alum1);
        System.out.println("La nota mayor de alum1 es: " + notaMayorAlum1);
        
        int notaMayorAlum2 = gestor.mostrarNotaMasAlta(alum2);
        System.out.println("La nota mayor de alum2 es: " + notaMayorAlum2);
        
        float promedioAlum1 = gestor.mostrarPromedioNota(alum1);
        System.out.println("El promedio de alum1 es: " + promedioAlum1);
        
        float promedioAlum2 = gestor.mostrarPromedioNota(alum2);
        System.out.println("El promedio de alum2 es: " + promedioAlum2);
                
        int notaBajaAlum1 = gestor.mostrarNotaMasBaja(alum1);
        System.out.println("La nota mas baja del alum1 es: " + notaBajaAlum1);
        
        int notaBajaAlum2 = gestor.mostrarNotaMasBaja(alum2);
        System.out.println("La nota mas baja del alum2 es: " + notaBajaAlum2);
        
        int notaMasAlta = gestor.mostrarNotaMasAltaAmbos(alum1, alum2);
        System.out.println("La nota mas alta entre alum1 y alum2 es: " + notaMasAlta);
        
        String alumNotaMasAlta = gestor.mostrarAlumnoNotaAlta(alum1, alum2);
        System.out.println("El alumno con la nota mas alta es: " + alumNotaMasAlta);
                
        float promAlum1 = gestor.mostrarPromedioNotaArreglo(alum1);
        System.out.println("El promedio de alum1 POR ARREGLO es: " + promAlum1);
        
        float promAlum2 = gestor.mostrarPromedioNotaArreglo(alum2);
        System.out.println("El promedio de alum2 POR ARREGLO es: " + promAlum2);
        
        
        
    } //Fin del main
    
} //Fin de la clase
