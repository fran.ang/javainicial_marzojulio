package paquete;

public class Principal {

    public static void main(String[] args) {
        
        //== EJEMPLO DE AVISO DE IMPUESTO ==
        //ENTRADAS
        int sueldo = 2000;
        
        //PROCESO
        if ( sueldo >= 3000 ){
            System.out.println("La persona debe abonar impuestos.");
        } else {
            System.out.println("La persona NO debe abonar impuestos.");
        }
        
        
        //== EJEMPLO DE INTERVALOS DE NUMEROS ==
        /*
        
        ----(--------)-------
            2        5
        */
        int numero = 4;
        
        if ( numero > 2 && numero < 5 ){
            System.out.println("El numero " + numero + " forma parte del intervalo.");
        } else {
            System.out.println("El numero ingresado NO forma parte del intervalo.");
        }
        
        
        //== EJEMPLO DE CANT. DE NUMEROS DE ACUERDO AL MES ==
        
        int mes = 11;
        
        if ( mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 
                || mes == 10 || mes == 12){
            
            System.out.println("El mes tiene 31 dias.");
            
        } else if( mes == 4 || mes == 6 || mes == 9 || mes == 11){
            
            System.out.println("El mes tiene 30 dias.");
            
        } else{
            
            System.out.println("El mes tiene 28 o 29 dias.");
        }                
        
    } //Fin del main
    
} //Fin de la clase
