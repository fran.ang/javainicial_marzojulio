package paquete;

public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //ENTRADAS
        int numero = 17;
        int resto = numero % 2;

        //PROCESO
        if ( resto == 0 ) {
            System.out.println("El numero " + numero + " es PAR");            
        } else {
            System.out.println("El numero " + numero + " es IMPAR");
        }
                

    } //Termina el main

} //Termina la clase
