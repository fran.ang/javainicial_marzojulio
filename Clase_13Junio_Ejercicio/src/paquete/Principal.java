package paquete;

public class Principal {

    public static void main(String[] args) {
        
        //Crear tres pacientes
        Paciente paciente1 = new Paciente(897, "Juan Pablo", true);
        Paciente paciente2 = new Paciente(459, "Maria", false);
        Paciente paciente3 = new Paciente(999, "Lisandro", true);
        
        //Instanciar un objeto de tipo Hospital
        Hospital hosp = new Hospital();
        
        //Llamar los metodos solicitados
        String mostObrSoc = hosp.mostrarSoloObraSocial(paciente1, paciente2, paciente3);
        System.out.println("Listado de nro de obras sociales: " + mostObrSoc);
        
        int sumaNroObrSoc = hosp.mostrarSumaObraSocial(paciente1, paciente2, paciente3);
        System.out.println("La suma de los nros. de obra social es: " + sumaNroObrSoc);
        
        int mostrarCantHistClin = hosp.mostrarCantHistClin(paciente1, paciente2, paciente3);
        System.out.println("En el hospital hay " + mostrarCantHistClin + " pacientes con historial clinica.");
                
        int mostrarCantNoHistClin = hosp.mostrarCantNoHistClin(paciente1, paciente2, paciente3);
        System.out.println("En el hospital hay " + mostrarCantNoHistClin + " pacientes que NO TIENEN historial clinica.");
        
        String mostrarArregloPacientes = hosp.mostrarArregloPacientes(paciente1, paciente2, paciente3);
        System.out.println("Listado de pacientes en el hospital: " + mostrarArregloPacientes);
        
        
        
        
    } //Fin del metodo main
    
} //Fin de la clase
