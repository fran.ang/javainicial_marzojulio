package paquete;

public class Paciente {
    
    private int nroObraSocial;
    private String nombre;
    private boolean histClinica; //Si tiene Historial clinica

    //Constructor
    public Paciente(int nroObraSocial, String nombre, boolean histClinica) {
        this.nroObraSocial = nroObraSocial;
        this.nombre = nombre;
        this.histClinica = histClinica;
    }

    public int getNroObraSocial() {
        return nroObraSocial;
    }

    public void setNroObraSocial(int nroObraSocial) {
        this.nroObraSocial = nroObraSocial;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean tieneHistClinica() {
        return histClinica;
    }

    public void setHistClinica(boolean histClinica) {
        this.histClinica = histClinica;
    }

    @Override
    public String toString() {
        return "Paciente{ " + "Numero Obra Social = " + nroObraSocial + 
                ", Nombre = " + nombre + 
                ", Tiene historial clinica? " + histClinica + " }";
    }        
}
