package paquete;

public class Hospital {        
    
    public String mostrarSoloObraSocial(Paciente pac1, Paciente pac2, Paciente pac3){
        String cadena = "";
        
        int nrosObraSocial[] = new int[3];
        nrosObraSocial[0] = pac1.getNroObraSocial();
        nrosObraSocial[1] = pac2.getNroObraSocial();
        nrosObraSocial[2] = pac3.getNroObraSocial();              
        
        for(int numero : nrosObraSocial){
            cadena += numero + " "; //cadena = cadena + numero + " ";            
        }
        
        return cadena;
    }
    
    public int mostrarSumaObraSocial(Paciente pac1, Paciente pac2, Paciente pac3){
        int suma = 0;
        
        int nrosObraSocial[] = new int[3];
        nrosObraSocial[0] = pac1.getNroObraSocial();
        nrosObraSocial[1] = pac2.getNroObraSocial();
        nrosObraSocial[2] = pac3.getNroObraSocial();
        
        for(int elemento : nrosObraSocial){
            suma += elemento;
        }
        
        return suma;
    }
    
    public int mostrarCantHistClin(Paciente pac1, Paciente pac2, Paciente pac3){
        int cantHistClinicos = 0;
        
        boolean histClinicos[] = new boolean[3];
        histClinicos[0] = pac1.tieneHistClinica();
        histClinicos[1] = pac2.tieneHistClinica();
        histClinicos[2] = pac3.tieneHistClinica();
        
        for( boolean elemento : histClinicos){
            if(elemento == true){
                cantHistClinicos++; //cantHistClinicos = cantHistClinicos + 1;
            }
        }
        
        return cantHistClinicos;
    }
    
    //TAREA
    //Mostrar la cantidad de pacientes que NO TIENEN hist. clinico
    public int mostrarCantNoHistClin(Paciente pac1, Paciente pac2, Paciente pac3){
        int cant = 0;
        
        boolean histClin[] = new boolean[3];
        histClin[0] = pac1.tieneHistClinica();
        histClin[1] = pac2.tieneHistClinica();
        histClin[2] = pac3.tieneHistClinica();
        
        for (boolean elemento : histClin) {
            if(elemento == false){
                cant++;
            }
        }
        
        return cant;
    }
    
    public String mostrarArregloPacientes(Paciente pac1, Paciente pac2, Paciente pac3){
        String cadena = "";
        
        Paciente pacientes[] = new Paciente[3];
        pacientes[0] = pac1;
        pacientes[1] = pac2;
        pacientes[2] = pac3;
        
        for (Paciente pac : pacientes) {
            cadena += pac.toString() + "\n";
        }
        
        return cadena;
    }
    
} //Fin de la clase
