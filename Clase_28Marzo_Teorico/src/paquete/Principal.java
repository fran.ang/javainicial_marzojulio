package paquete;

public class Principal {

    public static void main(String[] args) {

        //ATAJO: sout + Tecla TAB
        System.out.println("Hola mundo");

        // ======= ACTIVIDADES =======
        //=== EJERCICIO 1 === 
        //ENTRADAS
        int lado1 = 10;
        int lado2 = 12;
        int lado3 = 8;

        //PROCESO
        int perimetro = lado1 + lado2 + lado3;

        //SALIDA
        System.out.println("El perimetro es: " + perimetro);

        //=== EJERCICIO 1 con float === 
        //ENTRADAS
        float lado1f = 10.5f;
        float lado2f = 12.6f;
        float lado3f = 8.9f;

        //PROCESO
        float perimetrof = lado1f + lado2f + lado3f;

        //SALIDA
        System.out.println("El perimetro es: " + perimetrof);

        //=== EJERCICIO 2 === 
        //ENTRADA
        int nota1 = 10;
        int nota2 = 7;
        int nota3 = 10;

        //PROCESO
        int promedio = (nota1 + nota2 + nota3) / 3;

        //SALIDA
        System.out.println("El promedio es: " + promedio);
        //Se concatena (+) la cadena de texto con el valor de la variable

        //=== EJERCICIO 3 === 
        //ENTRADA
        int cantRtaCorrectas = 5;
        int cantRtaIncorrectas = 3;

        //PROCESO
        int ptjeFinal = cantRtaCorrectas * 4 - cantRtaIncorrectas;

        //SALIDA
        System.out.println("Cant. Rtas Correctas: " + cantRtaCorrectas);
        System.out.println("Cant. Rtas Incorrectas: " + cantRtaIncorrectas);
        System.out.println("Puntaje final: " + ptjeFinal + " puntos");

        
        //=== EJERCICIO 4 === 
        // ENTRADAS
        int nro1 = 10;
        int nro2 = 200;

        // PROCESOS Y SALIDA
        if (nro1 > nro2) {
            int mayor = nro1;
            System.out.println("El mayor es: " + mayor);
        } else {
            int mayor = nro2;
            System.out.println("El mayor es: " + mayor);
        }

    } //Termina el main

} //Termina la clase

