package paquete;

public class Principal {       
    
    public static void main(String[] args) {
        
        //SUBPROBLEMA 1
        int nro1 = 200;
        int nro2 = 300;
        int nro3 = 500;
        int numMenor = buscarMenor(nro1, nro2, nro3);
        System.out.println("El numero menor es: " + numMenor);
        
        //SUBPROBLEMA 2
        int cuadrado = calcularCuadrado( numMenor );
        System.out.println("El cuadrado del menor es: " + cuadrado);
        
        int cubo = calcularCubo( numMenor );
        System.out.println("El cubo del menor es: " + cubo);
                        
    } //Termina el main
    
    public static int buscarMenor( int nro1, int nro2, int nro3 ){        
        int menor = -1;
                        
        //PROCESO
        if( nro1 < nro2 && nro1 < nro3 ){
            menor = nro1; //Asginar un nuevo valor         
        }else if( nro2 < nro3 ){
            menor = nro2;
        }else{
            menor = nro3;            
        }
        
        //SALIDA        
        return menor;
    }
    
    public static int calcularCuadrado( int menor ){        
        int cuad = menor * menor;
        return cuad;
    }
    
    public static int calcularCubo( int menor ){
        int cubo = menor * menor * menor;
        return cubo;
    }
    
} //Termina la clase
