package paquete;

/**
 *
 * @author franc
 */
public class Auto extends Vehiculo {
    
    private boolean descapotable;

    public Auto(boolean descapotable, String patente, String propietario, int cantPuertas) {
        super(patente, propietario, cantPuertas);
        this.descapotable = descapotable;
    }        

    public boolean esDescapotable() {
        return descapotable;
    }

    public void setDescapotable(boolean descapotable) {
        this.descapotable = descapotable;
    }

    @Override
    public String toString() {
        return "Auto{" + "descapotable=" + descapotable + '}' + super.toString();
    }        
    
}
