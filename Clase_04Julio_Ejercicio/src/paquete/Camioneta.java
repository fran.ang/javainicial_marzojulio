
package paquete;

/**
 *
 * @author franc
 */
public class Camioneta extends Vehiculo {
    
    private float carga;

    public Camioneta(float carga, String patente, String propietario, int cantPuertas) {
        super(patente, propietario, cantPuertas);
        this.carga = carga;
    }
    
    public float getCarga() {
        return carga;
    }

    public void setCarga(float carga) {
        this.carga = carga;
    }

    @Override
    public String toString() {
        return "Camioneta{" + "carga=" + carga + '}' + super.toString();
    }            
}
