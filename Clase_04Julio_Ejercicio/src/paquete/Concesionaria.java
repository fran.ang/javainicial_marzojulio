package paquete;

/**
 *
 * @author franc
 */
public class Concesionaria {
    
    private Auto[] autos;
    private Camioneta[] camionetas;
    
    public Concesionaria(){
        autos = new Auto[2];
        camionetas = new Camioneta[2];
    }
    
    public void setAuto(Auto au, int i){
        autos[i] = au;
    }
    
    public void setCamioneta(Camioneta cam, int i){
        camionetas[i] = cam;
    }
    
    public int getCantidadAutos(){
        return autos.length;
    }
    
    public int getCantidadCamionetas(){
        return camionetas.length;
    }
    
    //Se debe mostrar todos los vehiculos
    public String mostrarVehiculos(){
        String cadena = " == LISTADO DE VEHICULOS == \n";
        
        //Recorremos el arreglo autos
        for(Auto au : autos){
            cadena += au.toString() + "\n";
        }
        
        //Recorremos el arreglo camionetas
        for(Camioneta cam : camionetas){
            cadena += cam.toString() + "\n";
        }        
        
        return cadena;
    }
    
    //Buscar el propietario ingresando la patente
    public String mostrarPropietario(String patente){
        String propietario = "";
        
        //Recorrer autos
        for(Auto au : autos){
            if(au.getPatente().equals(patente)){
                propietario = au.getPropietario();
            }
        }
        
        //Recorrer camionetas
        for(Camioneta cam : camionetas){
            if(cam.getPatente().equals(patente)){
                propietario = cam.getPropietario();
            }
        }
        
        return propietario;
    }
    
    //Sumar la cantidad de puertas de todos los vehiculos
    public int sumaCantPuertas(){
        int cantidad = 0;
        
        //Recorrer autos
        for(Auto au : autos){
            cantidad += au.getCantPuertas();
        }
        
        //Recorrer camionetas
        for(Camioneta cam : camionetas){
            cantidad += cam.getCantPuertas();
        }
        
        return cantidad;
    }
    
    //Mostrar la cantidad de autos descapotables
    public int cantAutosDescapotables(){
        int cantidad = 0;
        
        //Recorrer el arreglo de autos
        for(Auto au : autos){
            if( au.esDescapotable() == true ){
                cantidad++;
            }
        }
        
        return cantidad;
    }
    
    //TAREA - mostrar la cantidad de autos no descapotables
    public int cantAutosNoDescapotables(){
        int cantidad = 0;
        
        return cantidad;
    }
    
    //Mostrar un mensaje de exceso de carga si la suma total 
    //de las cargas de todas las camionetas superan los 500 kg
    public String mensajeAlertaCarga(){
        String mensaje = "";
        float sumaCargas = 0.0f;
        
        //Recorrer solo camionetas
        for(Camioneta cam : camionetas){
            sumaCargas += cam.getCarga();
        }
        
        if( sumaCargas > 500 ){
            mensaje = "Hay un exceso de cargas porque la suma total es " + sumaCargas;
        }else{
            mensaje = "No hay un exceso de cargas porque la suma total es " + sumaCargas;
        }
        
        return mensaje;
    }
}
