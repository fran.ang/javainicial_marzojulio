package paquete;

public class Principal {

    public static void main(String[] args) {
        
        //Crear cuatro obtejos en total
        Auto auto1 = new Auto(true, "AA123BB", "Juan Pablo", 4);
        Auto auto2 = new Auto(true, "AB321CC", "Maria", 2);
        Camioneta cam1 = new Camioneta(135.15f, "CC123DD", "Martin Lopez", 4);
        Camioneta cam2 = new Camioneta(166.99f, "CC321FF", "Lucia", 4);
        
        //Crear un objeto de tipo Concesionaria
        Concesionaria cons = new Concesionaria();
        
        //Cargar los arreglos de objetos
        cons.setAuto(auto1, 0);
        cons.setAuto(auto2, 1);
        cons.setCamioneta(cam1, 0);
        cons.setCamioneta(cam2, 1);
        
        //Llamar los metodos solicitados
        String mostrarVehiculos = cons.mostrarVehiculos();
        System.out.println(mostrarVehiculos);
        
        System.out.println("Ingresar una patente: ");
        String patente = Consola.readLine();
        String mostrarPropietario = cons.mostrarPropietario(patente);
        System.out.println("El propietario de esa patente es: " + mostrarPropietario);
        
        int sumaCantPuertas = cons.sumaCantPuertas();
        System.out.println("La cantidad de puertas de los vehiculos es: " + sumaCantPuertas);
        
        int cantAutosDescap = cons.cantAutosDescapotables();
        System.out.println("La cantidad de autos descapotables son: " + cantAutosDescap);
        
        String mensajeAlertaCarga = cons.mensajeAlertaCarga();
        System.out.println(mensajeAlertaCarga);
        
    } //Fin del metodo main
    
} //Fin de la clase
