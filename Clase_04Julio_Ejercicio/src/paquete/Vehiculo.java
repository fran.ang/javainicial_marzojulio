package paquete;

/**
 *
 * @author franc
 */
public class Vehiculo {
    
    //ATRIBUTOS
    private String patente;
    private String propietario;
    private int cantPuertas;

    //CONSTRUCTOR
    public Vehiculo(String patente, String propietario, int cantPuertas) {
        this.patente = patente;
        this.propietario = propietario;
        this.cantPuertas = cantPuertas;
    }

    //METODOS
    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public int getCantPuertas() {
        return cantPuertas;
    }

    public void setCantPuertas(int cantPuertas) {
        this.cantPuertas = cantPuertas;
    }

    @Override
    public String toString() {
        return " Patente = " + patente + 
                ", Propietario = " + propietario + 
                ", Cantidad de puertas = " + cantPuertas;
    }    
}
