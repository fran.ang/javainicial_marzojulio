package paquete;

public class Hospital {        
    
    public String buscarNombre(Paciente pac1, Paciente pac2, Paciente pac3, int nroObraSocial){
        String nombre = "";
        
        Paciente pacientes[] = new Paciente[3];
        pacientes[0] = pac1;
        pacientes[1] = pac2;
        pacientes[2] = pac3;
        
        //Recorrer el arreglo: Acceder a cada elemento (objeto) del arreglo.
        for(Paciente pac : pacientes){
            if(pac.getNroObraSocial() == nroObraSocial){
                //Si encontramos al paciente, guardamos su nombre
                nombre = pac.getNombre();
            }
        }
        
        return nombre;
    }
    
    public int buscarNroObraSoc(Paciente pac1, Paciente pac2, Paciente pac3, String nombre){
        int nroObraSoc = 0;  
        
        Paciente pacientes[] = new Paciente[3];
        pacientes[0] = pac1;
        pacientes[1] = pac2;
        pacientes[2] = pac3;
        
        // Tipo nombreElemento : arreglo
        for(Paciente pac : pacientes){
            if( pac.getNombre().equalsIgnoreCase(nombre) ){
                nroObraSoc = pac.getNroObraSocial();
            }
        }
        
        return nroObraSoc;
    }
    
    //TAREA - SOLUCION PROPUESTA
    public boolean tieneHistorialClinico(Paciente pac1, Paciente pac2, Paciente pac3, int nroObraSocial){
        boolean histClin = false;
        
        Paciente pacientes[] = new Paciente[3];
        pacientes[0] = pac1;
        pacientes[1] = pac2;
        pacientes[2] = pac3;
        
        for (Paciente pac : pacientes) {
            if(pac.getNroObraSocial() == nroObraSocial){
                histClin = pac.tieneHistClinica();
            }
        }
        
        return histClin;
    }
    
    public String mostrarPacientes(Paciente pac1, Paciente pac2, Paciente pac3){
        String cadena = "=== LISTADO DE PACIENTES ==\n";
        
        //Crear un arreglo de objetos: tipo pacientes[] = new tipo[3];
        Paciente pacientes[] = new Paciente[3];
        pacientes[0] = pac1;
        pacientes[1] = pac2;
        pacientes[2] = pac3;
        
        for(Paciente pac : pacientes){
            cadena += pac.toString() + "\n"; // \n hace salto de linea para ver un paciente debajo de otro
        }
        
        return cadena;
    }
    
    //TAREA
    //Buscar a un paciente segun su obra social. 
    // Si lo encuentra, debe devolver toda la informacion: nro obra social, nombre y si tiene historial
    // Si no lo encuentra, informar 'EL PACIENTE NO ESTA REGISTRADO'
    public String mostrarInfoPaciente(Paciente pac1, Paciente pac2, Paciente pac3, int nroObraSocial){
        String infoPaciente = "EL PACIENTE NO ESTA REGISTRADO.";
        
        Paciente pacientes[] = new Paciente[3];
        pacientes[0] = pac1;
        pacientes[1] = pac2;
        pacientes[2] = pac3;
        
        for (Paciente pac : pacientes) {
            if(pac.getNroObraSocial() == nroObraSocial){
                infoPaciente = pac.toString() + "\n";
            }
        }
        
        return infoPaciente;
    }
    
} //Fin de la clase
