package paquete;

public class Principal {

    public static void main(String[] args) {
        
        //Crear tres pacientes
        Paciente pac1 = new Paciente(9955, "Juan Pablo", true);
        Paciente pac2 = new Paciente(2244, "Maria", false);
        Paciente pac3 = new Paciente(7766, "Martin", false);
        
        //Instanciar un objeto de tipo Hospital
        Hospital hosp = new Hospital();
        
        //Llamar los metodos solicitados
        String mostrarPacientes = hosp.mostrarPacientes(pac1, pac2, pac3);
        System.out.println("Mostrar pacientes: " + mostrarPacientes);
        
        System.out.println("== BUSCAR NOMBRE DEL PACIENTE ==");
        System.out.println("Ingresar el numero de obra social: ");
        int nroObraSocial = Consola.readInt();
        String buscarNombre = hosp.buscarNombre(pac1, pac2, pac3, nroObraSocial);
        System.out.println("El nombre del paciente es: " + buscarNombre);
        
        System.out.println("== BUSCAR OBRA SOCIAL DEL PACIENTE ==");
        System.out.println("Ingresar el nombre del paciente: ");
        String nombre = Consola.readLine();
        int buscarNroObraSoc = hosp.buscarNroObraSoc(pac1, pac2, pac3, nombre);
        System.out.println("El nro de obra social del paciente es: " + buscarNroObraSoc);
        
        System.out.println("== BUSCAR HISTORIAL CLINICA DEL PACIENTE ==");
        System.out.println("Ingresar el numero de obra social: ");
        int nroObraSocial2 = Consola.readInt();
        boolean tieneHistorialClinico = hosp.tieneHistorialClinico(pac1, pac2, pac3, nroObraSocial2);
        System.out.println("¿El paciente tiene un historial clinico? " + tieneHistorialClinico);
        
        System.out.println("== INFORMACION DEL PACIENTE ==");
        System.out.println("Ingresar el numero de obra social: ");
        int nroObraSocial3 = Consola.readInt();
        String mostrarInfoPaciente = hosp.mostrarInfoPaciente(pac1, pac2, pac3, nroObraSocial3);
        System.out.println("Informacion del paciente: " + mostrarInfoPaciente);
        
        
        
        
    } //Fin del metodo main
    
} //Fin de la clase
