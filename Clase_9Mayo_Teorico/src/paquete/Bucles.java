package paquete;

public class Bucles {
    
    //NO POSEE VARIABLES DE CLASES
    
    //METODOS:
    public void cicloWhile(){
        
        //int nota1 = 10;
        //int nota2 = 9;
        //int nota3 = 10;
        //int cantidad = 3;
        
        //int promedio = (nota1 + nota2 + nota3) / cantidad;
        int suma = 0;
        int contador = 0;

        System.out.println("Ingrese una nota: ");
        int nota = Consola.readInt();
        
        while( nota != 0 ){
            suma += nota; //Acumulador
            contador++; //Contador
            
            System.out.println("Ingrese otra nota: ");
            nota = Consola.readInt();
        }
        
        System.out.println("Suma = " + suma);
        System.out.println("Contador = " + contador);
        
        int promedio = suma / contador;
        System.out.println("El promedio es: " + promedio);        
        
    } //Fin del metodo
    
} //Fin de la clase
