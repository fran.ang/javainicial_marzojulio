package paquete;

public class Principal {

    public static void main(String[] args) {
        
        //Entradas
        int nro1 = 4;
        int nro2 = 9;
        
        int mayor = buscarMayor(nro1, nro2);
        System.out.println("El mayor es: " + mayor);
        
        int menor = buscarMenor(nro1, nro2);
        System.out.println("El menor es: " + menor);
        
        int areaCuad = areaCuadrado(mayor);
        System.out.println("El area del cuadrado es: " + areaCuad);
        
        float areaCirc = areaCirculo(menor);
        System.out.println("El area del circulo es: " + areaCirc);
        
    } //Termina el main
    
    public static int buscarMayor( int numero1, int numero2 ){
        int mayor = 0;        
        
        if( numero1 > numero2 ){
            mayor = numero1;
        } else{
            mayor = numero2;
        }
        
        return mayor;
    } //Termina metodo
    
    public static int buscarMenor( int numero1, int numero2 ){
        int menor = 0;
        
        if( numero1 < numero2 ){
            menor = numero1;
        }else{
            menor = numero2;
        }
        
        return menor;
    } //Termina metodo
    
    public static int areaCuadrado( int lado ){
        int area = lado * lado;
        return area;
    } //Termina metodo
    
    public static float areaCirculo( int radio ){        
        float area = 3.14f * radio * radio;
        return area;
    } //Termina metodo
    
} //Termina mi clase
